source /etc/profile

IMAGES=$1
SERVICE_NAME=$2
HOST=$3
PORT=$4
cPath=.

echo "stop current running program firstly"
echo "starting program: $SERVICE_NAME"
echo "host $HOST"
echo "port $PORT"
echo "image $IMAGES"
echo "servicename $SERVICE_NAME"
$(aws ecr get-login --no-include-email --region ap-south-1)
docker pull $IMAGES:latest
docker ps -f name=$SERVICE_NAME -q | xargs --no-run-if-empty docker container stop
docker container ls -a -fname=$SERVICE_NAME -q | xargs -r docker container rm
docker run --restart=always -d  --name $SERVICE_NAME  -p $PORT:80 -e "EUREKA_INSTANCE_IP-ADDRESS=$HOST" -e "SERVER_PORT=$PORT"  $IMAGES
echo "program is started"
