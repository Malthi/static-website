import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.themoviedb.org/3',
  params: {
    api_key: "844dba0bfd8f3a4f3799f6130ef9e335",
  },
});
